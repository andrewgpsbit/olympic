<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
      $now = Carbon::now();
      
      DB::table('countries')->insert([
        ['code' => 1, 'name' => 'Россия', 'created_at' => $now],
        ['code' => 2, 'name' => 'Канада', 'created_at' => $now],
        ['code' => 3, 'name' => 'Италия', 'created_at' => $now],
        ['code' => 4, 'name' => 'Греция', 'created_at' => $now],
        ['code' => 5, 'name' => 'Великобритания', 'created_at' => $now],
        ['code' => 6, 'name' => 'КНР', 'created_at' => $now],
      ]);

      DB::table('olympics')->insert([
        ['year' => 2004, 'place' => 'Афины', 'country_id' => 4, 'type' => 1, 'created_at' => $now],
        ['year' => 2006, 'place' => 'Турине', 'country_id' => 3, 'type' => 2, 'created_at' => $now],
        ['year' => 2008, 'place' => 'Пекин', 'country_id' => 6, 'type' => 1, 'created_at' => $now],
        ['year' => 2010, 'place' => 'Ванкувер', 'country_id' => 2, 'type' => 2, 'created_at' => $now],
        ['year' => 2012, 'place' => 'Лондоне', 'country_id' => 5, 'type' => 1, 'created_at' => $now],
        ['year' => 2014, 'place' => 'Сочи', 'country_id' => 1, 'type' => 2, 'created_at' => $now],
      ]);

      DB::table('type_results')->insert([
        ['name' => 'Вес', 'direct_reverse' => 1, 'created_at' => $now],
        ['name' => 'Высота', 'direct_reverse' => 1, 'created_at' => $now],
        ['name' => 'Очки', 'direct_reverse' => 1, 'created_at' => $now],
        ['name' => 'Баллы', 'direct_reverse' => 1, 'created_at' => $now],
        ['name' => 'Время', 'direct_reverse' => 2, 'created_at' => $now],
      ]);

      DB::table('discipline_defaults')->insert([
        ['name' => 'Плавание', 'olympic_id' => 6, 'type_result_id' => 5, 'created_at' => $now],
        ['name' => 'Прыжки в высоту', 'olympic_id' => 6, 'type_result_id' => 2, 'created_at' => $now],
        ['name' => 'Бег', 'olympic_id' => 6, 'type_result_id' => 5, 'created_at' => $now],
      ]);

      DB::table('discipline_additionals')->insert([
        ['name' => 'Плавание. Брасс. 200м. Мужчины', 'sex' => 1, 'discipline_default_id' => 1, 'created_at' => $now],
        ['name' => 'Плавание. Брасс. 200м. Женщины', 'sex' => 2, 'discipline_default_id' => 1, 'created_at' => $now],
        ['name' => 'Прыжки в высоту. 5м. Мужчины', 'sex' => 1, 'discipline_default_id' => 2, 'created_at' => $now],
        ['name' => 'Прыжки в высоту. 5м. Женщины', 'sex' => 2, 'discipline_default_id' => 2, 'created_at' => $now],
        ['name' => 'Бег. 1000м. Мужчины', 'sex' => 1, 'discipline_default_id' => 3, 'created_at' => $now],
        ['name' => 'Бег. 1000м. Женщины', 'sex' => 2, 'discipline_default_id' => 3, 'created_at' => $now],
      ]);

      DB::table('races')->insert([
        ['start' => $now, 'end' => $now->addHour(4), 'discipline_additional_id' => 1, 'created_at' => $now],
        ['start' => $now->addHour(4), 'end' => $now->addHour(8), 'discipline_additional_id' => 2, 'created_at' => $now],
        ['start' => $now->addDay(), 'end' => $now->addDay()->addHour(4), 'discipline_additional_id' => 3, 'created_at' => $now],
        ['start' => $now->addDay()->addHour(4), 'end' => $now->addDay()->addHour(8), 'discipline_additional_id' => 4, 'created_at' => $now],
        ['start' => $now->addDay(2), 'end' => $now->addDay(2)->addHour(4), 'discipline_additional_id' => 5, 'created_at' => $now],
        ['start' => $now->addDay(2)->addHour(4), 'end' => $now->addDay(2)->addHour(8), 'discipline_additional_id' => 6, 'created_at' => $now],
      ]);

      DB::table('athletes')->insert([
        ['name' => 'Майкл Фелпс', 'sex' => 1],
        ['name' => 'Лариса Латынина', 'sex' => 2],
        ['name' => 'Пааво Нурми', 'sex' => 1],
        ['name' => 'Марк Спитц', 'sex' => 1],
        ['name' => 'Карл Льюис', 'sex' => 1],
        ['name' => 'Изабель Верт', 'sex' => 2],
      ]);

      DB::table('registers')->insert([
        ['race_id' => 1, 'athlete_id' => 1],
        ['race_id' => 1, 'athlete_id' => 2],
        ['race_id' => 1, 'athlete_id' => 3],
        ['race_id' => 1, 'athlete_id' => 4],
      ]);
    }
}
