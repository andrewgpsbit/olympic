<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplineAdditionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discipline_additionals', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->smallInteger('sex');
            $table->integer('discipline_default_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('discipline_default_id')
                ->references('id')->on('discipline_defaults')
                ->onDelete('cascade');


            $table->index([
              'discipline_default_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discipline_additionals');
    }
}
