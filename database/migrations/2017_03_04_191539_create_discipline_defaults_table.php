<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplineDefaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discipline_defaults', function (Blueprint $table) {
          $table->increments('id');

          $table->string('name');
          $table->integer('olympic_id')->unsigned();
          $table->integer('type_result_id')->unsigned();

          $table->timestamps();
          $table->softDeletes();


          $table->foreign('olympic_id')
            ->references('id')->on('olympics')
            ->onDelete('cascade');

          $table->foreign('type_result_id')
            ->references('id')->on('type_results');


          $table->index([
            'olympic_id',
            'type_result_id',
          ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discipline_defaults');
    }
}
