<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->increments('id');

            $table->float('result', 8, 3)->nullable();
            $table->integer('race_id')->unsigned();
            $table->integer('athlete_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('race_id')
              ->references('id')->on('races')
              ->onDelete('cascade');

            $table->foreign('athlete_id')
              ->references('id')->on('athletes')
              ->onDelete('cascade');


            $table->index([
              'race_id',
              'athlete_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
