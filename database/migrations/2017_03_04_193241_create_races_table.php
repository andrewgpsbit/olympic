<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('races', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('start');
            $table->dateTime('end');
            $table->integer('discipline_additional_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('discipline_additional_id')
                ->references('id')->on('discipline_additionals')
                ->onDelete('cascade');


            $table->index([
              'discipline_additional_id',
            ]);
         ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('races');
    }
}
