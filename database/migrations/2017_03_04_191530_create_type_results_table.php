<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->smallInteger('direct_reverse');
            $table->timestamps();

            $table->index([
              'direct_reverse'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_results');
    }
}
