<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlympicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olympics', function (Blueprint $table) {
          $table->increments('id');

          $table->smallInteger('year');
          $table->string('place');
          $table->smallInteger('type');
          $table->integer('country_id')->unsigned();

          $table->timestamps();
          $table->softDeletes();

          $table->foreign('country_id')
            ->references('id')->on('countries')
            ->onDelete('cascade');


          $table->index([
            'country_id'
          ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olympics');
    }
}
