<?php

namespace App\Services\Country;

use App\Models\Country;

class CountryHandle
{

    public function showAll(){

        $countries = Country::get()->toArray();

        return view('home.index', ['countries' => $countries]);

    }


}
