<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Country\CountryHandle;

class CommonController extends Controller
{
    public function index() {

        $coutry = new CountryHandle();

        return $coutry->showAll();

    }
}
