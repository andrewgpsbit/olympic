<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }
    </style>
</head>
<body>

<div class="container">
    <h2>Доступные страны</h2>
    <p>The .table-bordered class adds borders to a table:</p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Код</th>
            <th>Название</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @foreach ($countries as $countrie)
            <tr data-id="{{ $countrie['id'] }}">
                <td>{{ $countrie['code'] }}</td>
                <td>{{ $countrie['name'] }}</td>
                <td>Редактировать</td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>

</body>
</html>
